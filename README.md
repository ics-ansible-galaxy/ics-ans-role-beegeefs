ics-ans-role-beegeefs
===================

Ansible role to install beegeefs.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-beegeefs
```

License
-------

BSD 2-clause
